console.log('正在下载更新，请勿关闭此窗口...');

const { update } = require('./update-electron.js');
update().then(() => {
    console.log('下载完成. 正在安装...');
    process.exit();
}).catch(e => {
    console.log(e);
});
